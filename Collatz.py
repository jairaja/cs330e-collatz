#!/usr/bin/env python3

from collections import Counter


# ------------
# collatz_read
# ------------


def collatz_read(s):
    """
    read the details about an army
    s a string
    return a list of three/four strings: an army, a location, and an action, argument for Support or move
    """
    a = s.split()
    return a


def find_winner_army(armies, support_counter):
    """
    armies is list of armies in same city
    support_counter contains the count of each of their support
    returns winner army else returns "No one wins"
    """
    if len(armies) == 1:
        return armies[0]
    
    max_supp = 0
    for army in armies:
        max_supp = max(max_supp, support_counter[army])

    num_max = 0
    for army in armies:
        if max_supp == support_counter[army]:
            num_max += 1
            winner = army

    if num_max > 1:
        return "No one wins"

    return winner


# ------------
# collatz_eval
# ------------


def collatz_eval(information):
    """
    information list of list of strings
    return the dictionary where keys are armies and values are locations
    """
    actions = ["Hold", "Move", "Support"]
    city_to_armies = {}

    for info in information:
        assert info[2] in actions

        if info[2] != "Move":
            city = info[1]
        else:
            city = info[3]

        if city not in city_to_armies:
            city_to_armies[city] = [] 

        city_to_armies[city].append(info[0])
        
    support_counter = Counter()    
    for info in information:
        if info[2] == "Support" and len(city_to_armies[info[1]]) == 1:
            support_counter[info[3]] += 1

    army_city = {}
    for city in city_to_armies:
        winner = find_winner_army(city_to_armies[city], support_counter)
        for army in city_to_armies[city]:
            if army == winner:
                army_city[army] = city

            else:
                army_city[army] = "[dead]"

    return army_city

# -------------
# collatz_print
# -------------


def collatz_print(w, locations):
    """
    print location of each army in a new line
    locations is list of armies and their locations
    """
    for loc in locations:
        w.write(str(loc) + " " + str(locations[loc]) + "\n")

# -------------
# collatz_solve
# -------------


def collatz_solve(r, w):
    """
    r a reader
    w a writer
    """
    for s in r:
        test_cases = int(s)
        break

    new_case = 1
    armies = []

    for s in r:
        if new_case:
            if len(armies):
                locations = collatz_eval(armies)
                collatz_print(w, locations)
            num_armies = int(s)
            new_case = 0
            counter = num_armies
            armies = []
            continue

        army = collatz_read(s)
        armies.append(army)
        counter -= 1
        if not counter:
            new_case = 1
    if len(armies):
        locations = collatz_eval(armies)
        collatz_print(w, locations)
